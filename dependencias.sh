echo " "
echo "-----------------------------------------------------------"
echo "Instalando certbot"
echo "-----------------------------------------------------------"
echo " "

sudo snap install core
sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot



echo "-----------------------------------------------------------"
echo "Descargando docker"
echo "-----------------------------------------------------------"
echo " "
sudo apt-get update


echo " "
echo "-----------------------------------------------------------"
echo "Instalando Docker"
echo "-----------------------------------------------------------"
echo " "

sudo apt install docker.io -y


echo "-----------------------------------------------------------"
echo "Instalando docker compose"
echo "-----------------------------------------------------------"

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

echo " "
echo "-----------------------------------------------------------"
echo "Configurando docker"
echo "-----------------------------------------------------------"
echo " "
sudo usermod -aG docker $USER

echo "  "
echo "-----------------------------------------------------------"
echo "Configurando servicios docker"
echo "-----------------------------------------------------------"
echo "  "

sudo systemctl enable docker.service
sudo systemctl enable containerd.service


echo " "

echo "Se finalizó la instalación existosamente (/._.)/"
echo " "


